#include <main.h>

unsigned int8 OE = 0;
int1 OT = 0;
int1 IE = 0;
unsigned int16 timer = 0;  
unsigned int16 NT = 179;

#INT_EXT
void  EXT_isr(void) {
   timer = get_timer1();
   set_timer1(0);
   OE = 64;
   IE = true;
}

#INT_TIMER0
void  TIMER0_isr(void) {
   if (!OE) {
      output_bit(OUT, 0);
      return;
   }
   OT = !OT;
   output_bit(OUT, OT);
   set_timer0(NT);
   if (OE) OE --;
}

void main() {
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256|RTCC_8_BIT);      //65,5 ms overflow
   setup_timer_1(T1_INTERNAL|T1_DIV_BY_8);      //524 ms overflow

   SET_TRIS_A( 0x04 );

   enable_interrupts(INT_EXT);
   enable_interrupts(INT_TIMER0);
   enable_interrupts(GLOBAL);

   while(TRUE) {
      if (IE) {
         IE = false;
         //NT = 255 - (timer / 77); //1:1
         NT = 255 - (timer / 131); //1:2
      }
   }

}
